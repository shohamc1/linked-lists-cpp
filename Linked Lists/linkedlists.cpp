#include <cstddef>
#include <iostream>
#include <cstdlib>
#include <cstdio>
using namespace std;

struct node {
	int data;
	node *next;
} *start;

class linkedlist {
public:
	node* create_node(int value) {
		node *temp, *s;
		temp = new (node);

		if (temp == NULL) {
			cout << "Memory not allocated.";
			exit(1);
		}

		else {
			temp->data = value;
			temp->next = NULL;
		}
	}

	void add_beginning() {
		int value;

		cout << "Enter value to be inserted: ";
		cin >> value;

		node *temp, *p;

		temp = create_node(value);

		if (start == NULL) {
			start = temp;
			start->next = NULL;
		}

		else {
			p = start;
			start = temp;
			start->next = p;
		}

		cout << "Element inserted at beginning!" << endl;

	}

	void add_end() {
		int value;
		cout << "Enter value to be inserted: ";
		cin >> value;

		node *temp, *s;

		temp = create_node(value);

		s = start;

		while (s->next != NULL) {
			s = s->next;
		}

		temp->next = NULL;
		s->next = temp;

		cout << "Element inserted at end!" << endl;
	}

	void add_pos() {
		int value, pos, counter = 0;

		cout << "Enter value to be added: ";
		cin >> value;

		node *temp, *s, *ptr;
		temp = create_node(value);

		cout << "Enter position where to add: ";
		cin >> pos;

		s = start;


		while (s != NULL) {
			s = s->next;
			counter++;
		}

		if (pos == 1) {
			if (start = NULL) {
				start = temp;
				start->next = NULL;
			}
			else {
				ptr = start;
				start = temp;
				start->next = ptr;
			}
		}

		else if (pos > 1 && pos <= counter) {
			s = start;

			for (int i = 1; i < pos; i++) {
				ptr = s;
				s = s->next;
			}

			ptr->next = temp;
			temp->next = s;
		}

		else {
			cout << "Position is out of range!" << endl;
		}

		cout << "Element added!\n";
	}

	void searchfn() {
		int value, pos = 0;
		bool x = 0;

		if (start == NULL) {
			cout << "List is empty!" << endl;
		}

		cout << "Enter the value to be searched: ";
		cin >> value;

		node *s;

		s = start;

		while (s != NULL) {
			pos++;

			if (s->data == value) {
				cout << value << " found at " << pos << "!" << endl;
				x = 1;
			}

			s = s->next;
		}

		if (s == 0) {
			cout << "Value not found!" << endl;
		}
	}

	void deletefn() {
		int pos = 0, counter = 0;

		if (start == NULL) {
			cout << "List is empty";
			exit(1);
		}

		cout << "Enter position to be deleted: ";
		cin >> pos;

		node *s, *ptr;

		s = start;

		if (pos == 1) {
			start = s->next;
		}

		else {
			while (s != NULL) {
				counter++;

				s = s->next;
			}

			if (pos > 1 && pos <= counter) {
				s = start;

				for (int i = 1; i < pos; i++) {
					ptr = s;
					s = s->next;

				}

				ptr->next = s->next;

			}

			else {
				cout << "Position out of range!" << endl;
			}

			delete s;

			cout << "Element deleted!";
		}
	}

	void display() {
		node *temp;

		if (start == NULL) {
			cout << "List is empty.";
			exit(1);
		}

		temp = start;

		cout << "Elements are: " << endl;

		while (temp != NULL) {
			cout << temp->data << "->";
			temp = temp->next;
		}

		cout << "NULL" << endl;
	}

	void reverse() {
		node *current = start;
		node *prev = NULL, *next = NULL;

		while (current != NULL) {
			next = current->next;
			current->next = prev;

			prev = current;
			current = next;
		}

		start = prev;
	}

	void middleterm() {
	    node *slow = start, *fast = start;

	    if (start != NULL) {
            while (fast != NULL && fast->next != NULL) {
                fast = fast->next->next;
                slow = slow->next;
            }
	    }

	    cout << "Middle is " << slow->data << ".\n";
	}
};


int main() {
	linkedlist l1;
	int choice;

	start = NULL;


	while (choice != 0) {
		cout << "1. Add node at beginning" << endl;
		cout << "2. Add node at end" << endl;
		cout << "3. Add at a position" << endl;
		cout << "4. Search" << endl;
		cout << "5. Delete node" << endl;
		cout << "6. Display list" << endl;
		cout << "7. Exit" << endl;
		cout << "8. Reverse" << endl;
		cout << "9. Middle term" << endl;

		cout << "What do you want to do?";
		cin >> choice;

		switch (choice) {
		case 1:
			l1.add_beginning();
			break;

		case 2:
			l1.add_end();
			break;

		case 3:
			l1.add_pos();
			break;

		case 4:
			l1.searchfn();
			break;

		case 5:
			l1.deletefn();
			break;

		case 6:
			l1.display();
			break;

		case 7:
			exit(0);
			break;

		case 8:
			l1.reverse();
			break;

        case 9:
            l1.middleterm();
            break;
		}
	}
}
