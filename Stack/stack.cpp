#include <iostream>
#define max 5
using namespace std;

int stack [max], top;

void initStack () {
    top = -1;
}

int isEmpty() {
    if (top == - 1)
        return 1;
    else
        return 0;
}

int isFull() {
    if (top == max - 1)
        return 1;
    else
        return 0;
}

void push (int num) {
    if (isFull()) {
        cout << "Stack is full!" << endl;
        return;
    }

    ++top;
    stack[top] = num;
    cout << num << " has been inserted!" << endl;
}

void display() {
    if (isEmpty()) {
        cout << "Stack is empty!" << endl;
        return;
    }

    for (int i = top; i >= 0; i--) {
        cout << stack[i] << endl;
    }
}

void pop() {
    int temp;
    if (isEmpty()) {
        cout << "Stack is empty!" << endl;
        return;
    }

    temp = stack[top];
    top--;

    cout << temp << " has been deleted!" << endl;
}

int main() {
    cout << "(C) 2018 SHOHAM CHAKRABORTY\n";

    int num, a;
    initStack();

    char ch;

    do {
        cout << "Choose: \n1.Push\n2.Pop\n3.Display\n";
        cout << "Enter choice: ";
        cin >> a;

        switch (a) {
            case 1:
                cout << "Enter integer number: ";
                cin >> num;
                push (num);
                break;
            case 2:
                pop();
                break;
            case 3:
                display();
                break;

            default:
                cout << "Invalid choice!" << endl;
        }

        cout << "Do you want to continue?";
        cin >> ch;
    } while (ch == 'Y'||ch == 'y');
}