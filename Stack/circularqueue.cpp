#include <iostream>
#include <climits>
using namespace std;

class queue {
public:
    int rear, front;

    int size, *arr;

    queue (int s) {
        front = rear = -1;
        size = s;
        arr = new int[s];
    }

    void enQueue (int value) {
        if ((front == 0 && rear == size-1) || (rear == (front - 1)%(size-1))) {
            cout << "Queue is full!\n";
            return;
        }

        else if (front == -1) {
            front = rear = 0;
            arr[rear] = value;
        }

        else if (rear == size - 1 && front != 0) {
            rear = 0;
            arr [rear] = value;
        }

        else {
            rear++;
            arr[rear] = value;
        }
    }

    void deQueue () {
        if (front == -1) {
            cout << "Queue is empty!\n";
            return;
        }

        int data = arr[front];
        arr[front] = -1;
        if (front == rear) {
            front = rear = -1;
        }

        else if (front == size - 1) {
            front = 0;
        }

        else {
            front++;
        }
    }

    void displayQueue() {
        if (front == -1) {
            cout << "Queue is empty!\n";
            return;
        }

        cout << "Elements are:\n ";

        if (rear >= front) {
            for (int i = front; i <= rear; i++) {
                cout << arr[i] << endl;
            }
        }

        else {
            for (int i = front; i < size; i++) {
                cout << arr[i] << endl;
            }

            for (int i = 0; i <= rear; i++) {
                cout << arr[i] << endl;
            }
        }
    }
};

int main() {
    int choice;
    
    cout << "Enter number of variables: ";
    cin >> choice;
    queue q(choice);

    for (;;) {
        cout << "\n1. Enqueue\n2. Dequeue\n3. Display\n4. Exit\nEnter choice: ";
        cin >> choice;

        switch (choice) {
            case 1:
                cout << "Enter number to be added: ";
                cin >> choice;
                q.enQueue(choice);
                break;
            case 2:
                q.deQueue();
                break;
            case 3:
                q.displayQueue();
                break;
            case 4:
                return 0;
            default:
                cout << "Wrong choice!\n";
        }
    }
}
