#include <iostream>
using namespace std;

struct node {
    int data;
    node *next;
};

class stackclass {
    public:

        node *top;

        stackclass() {
            top = NULL;
        }

        void push () {
            int value;
            cout << "Enter a number to insert: ";
            cin >> value;

            node *ptr;
            ptr = new node;
            ptr->data = value;
            ptr->next = NULL;

            if (top != NULL) {
                ptr->next = top;
            }

            top = ptr;

            cout << "Successfully pushed!\n";
        }

        void pop() {
            if (top == NULL) {
                cout << "Stack is empty!";
            }
            top = top->next;

            cout << "Successfully popped!\n";
        }

        void display() {
            node *ptr = top;
            cout << "Stack: \n";

            while (ptr != NULL) {
                cout << ptr->data << endl;
                ptr = ptr->next;
            }

            cout << "NULL\n";
        }
};

int main () {
    stackclass s;

    int choice;

    for (;;) {
        cout << "Enter choice: \n1. Push\n2. Pop\n3. Display\n4. Exit\n";
        cin >> choice;

        switch (choice) {
            case 1:
                s.push();
                break;
            case 2:
                s.pop();
                break;
            case 3:
                s.display();
                break;
            case 4:
                return 0;
            default:
                cout << "Wrong option!\n";
        }
    }
}