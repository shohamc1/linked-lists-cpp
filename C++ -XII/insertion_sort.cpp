#include<iostream>
using namespace std;


int main( )
{
    int arr[5];
    int i, j, small;
    int n=5; // number of elements
    cout<<"Using Insertion sort\n\n Before sorting \n\n ";
    for(i=0; i<n; i++) {
        cout<< arr[i];
        cout<<"\n";
    } 
        for(i=0; i<n; i++) { //number of passes
            small = arr[i];
            for(j=i-1; j>=0 && small< arr[j] ;j--) { // number of comparisons
                arr[j+1]=arr[j];
                small=j;	
            }	 
            arr[j] = small;
        } 
    cout<<"\n\n After sorting \n\n"<<endl;
    for(i=0; i<n; i++) {
        cout<< arr[i];
        cout<<"\n"; 
	} 
} 

