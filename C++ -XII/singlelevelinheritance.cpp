using namespace std;
  #include<iostream>
  
  class A
  { int i,j;
   public:
  A(int a, int b)
  { i=a; j=b;}
   Sample (Sample &s)
  { j=s.i; i=s.j;
  cout<<"\n Copy constructor working\n";
   }
int  print(void)
{  cout<<i<<" "<<j<<"\n";}
 };
 class B
  { int i,j;
   public:
  Sample(int a, int b)
{ i=a; j=b;}
Sample (Sample &s)
{ j=s.i; i=s.j;
  cout<<"\n Copy constructor working\n";
}
int  print(void)
{  cout<<i<<" "<<j<<"\n";}
 };
 
   int main()
  {
          Sample S1(4,5);   // S1 initialized first constructor used
          Sample S2(S1);   // S1 copied to S2. Copy constructor called.
	    Sample S3=S1;   // S1 copied to S3. Copy constructor called again.
		S2.print();
		S3.print();		
         
  }

