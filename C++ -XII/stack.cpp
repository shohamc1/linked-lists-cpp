/******************************************/
/*PROGRAM TO IMPLEMENT STACKS USING ARRAYS*/
/******************************************/

#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;

#define SIZE 5

void menu();
void display();
int underflow();
int overflow();
void push(int);
void pop();

int stack[SIZE];
int top=-1;

int main()
{
   // clrscr();
    menu();
    return 0;
}

void menu()
{
    int choice,item;
    cout<<"MENU";
    cout<<"\n1. Push into the stack";
    cout<<"\n2. Pop from stack";
    cout<<"\n3. Display";
    cout<<"\n4. Exit";
    cout<<"\nEnter your choice: ";
    cin>>choice;
    switch(choice)
    {
        case 1:
           // clrscr();
            if(overflow()==0)
            {
                cout<<"\nEnter the item tobe pushed: ";
                cin>>item;
                push(item);
              //  clrscr();
                cout<<"\nAfter push operation stack is:\n";
            }
            display();
        //    getch();
           // clrscr();
            menu();
            break;
        

case 2:
          //  clrscr();
            if(underflow()==1)
            {
                pop();
                if(underflow()==1)
                {
                    cout<<"\nAfter pop operation stack is:\n";
                    display();
                }
            }
        //    getch();
         //   clrscr();
            menu();
            break;
        case 3:
         //   clrscr();
            if(underflow()==1)
            {
                cout<<"The stack is:\n";
                display();
            }
          //  getch();
           // clrscr();
            menu();
            break;
        case 4:
            exit(1);
        default:
         //   clrscr();
            cout<<"Your choice is wrong\n\n";
            menu();
    }
}

int underflow()
{
    if(top==-1)
    {
        cout<<"\nStack is empty";
        return(0);
    }
    else
    {
        return(1);
    }
}

int overflow()
{
    if(top==SIZE-1)
    {
        cout<<"\nStack is full\n";
        return(1);
    }
    else
    {
        return(0);
    }
}

void push(int item)
{
    top=top+1;
    stack[top]=item;
}

void pop()
{
    top=top-1;
}

void display()
{
    int i;
    for(i=top;i>-1;i--)
    {
        cout<<"\nElement is "<<i+1<<"\t"<<stack[i];
    }
}


